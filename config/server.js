module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', '134455fbaa8986d7da3b9ed89d313b2c'),
    },
  },
  url: 'https://strapidemo.kvly.fr'
});
